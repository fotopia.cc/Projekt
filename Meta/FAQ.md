# FAQ
Die FAQ vom fotopia-Projekt.

## Wird den FotografInnen durch kostenlose Bilder nicht die Lebensgrundlage entzogen?
Kritischen Projekten fehlt leider oft das Geld, FotografInnen ordentlich zu entlohnen. Andererseits: FotografInnen sind oft bereit, ihre Fotos für den guten Zweck zu spenden. Um Missbrauch zu vermeiden, wird im Downloadprozess der Verwendungszweck des Fotos anzugeben sein. (z.B. Name der Publikation). Für FotografInnen soll absolute Transparenz darüber herrschen, wer welche Fotos lädt. Die UrheberInnen können dann verfolgen, wer ihre Fotos mit welchen Absichten in welchem Kontext verwendet und ob eine Entlohnung oder Spende gerechtfertig scheint.

## Wie stellt ihr sicher, dass nur progressive unkommerzielle Projekte die Bilder nutzen?
Es wird einen "Fotopia-Ehrencodex" geben, den alle NutzerInnen von fotopia.cc unterzeichnen. Mit ihm verpflichten sie sich, bei der Bildverwendung ethische Standards einzuhalten. Eindeutige Benutzerkonten befördern den verbindlichen und transparenten Umgang der NutzerInnen untereinander. Dabei messen wir auch der Ausarbeitung der AGB einen hohen Stellenwert bei. Totale Kontrolle halten wir hier allerdings für unmöglich.

## Gibt es so etwas wie fotopia nicht schon? Z. B. bekomme ich meine Fotos kostenlos bei GettyImages oder pixabay.
GettyImages stellt zu Marketingzwecken einige Fotos zur ausschließlichen Verwendung im Netz bereit. Diese Entscheidung kann Getty jederzeit ändern. Pixabay bietet Symbolfotos an. Es gibt auch andere kommerzielle Plattformen, bei denen man freie Inhalte findet, aber entweder sind die Lizenzbedingungen unklar oder die Inhalte liegen in der Hand großer Unternehmen. 
Bei fotopia.cc findest Du journalistische Fotos aus der Bewegung für die Bewegung. Wir wollen die Vermarktung der Bilder unseres Protests nicht Großkonzernen überlassen. Diese Idee ist bisher einzigartig.

## Verlieren die FotografInnen das Recht, ihre Bilder kommerziell zu nutzen?
Nein, die FotografInnen stellen die Bilder bei fotopia unter die CreativeCommons-Lizenz ihrer Wahl und können dabei selbst entscheiden, wie viele Rechte sie gewähren wollen. Es ist beispielsweise möglich, Bearbeitungen des Bildes oder den Einsatz für kommerzielle Zwecke explizit auszuschließen. Dabei verzichten die FotografInnen zu keinem Zeitpunkt auf ihr Recht, die Bilder anderweitig an zahlungskräftige Organisationen zu lizensieren. Im Downloadprozess wird zudem der Verwendungszweck anzugeben sein. Die UrheberInnen können dann verfolgen, wer ihre Fotos mit welchen Absichten in welchem Kontext verwendet und ob eine Entlohnung oder Spende gerechtfertig scheint.

## Ist meine Spende bei euch wirklich in guten Händen?
Ja, fotopia.cc ist ein eingetragener gemeinnütziger Verein. Wir arbeiten rein ehrenamtlich und dürfen die Spenden nur für fotopia.cc ausgeben. Das müssen wir auch jedes Jahr gegenüber dem Finanzamt nachweisen.

## Wie finanziert sich fotopia.cc langfristig?
Wir sehen, dass viele Plattformen rein von den Spenden leben können. Erst einmal wollen wir keinerlei Nutzungsgebühren erheben, denn jede*r soll selbst entscheiden können, ob und wie viel er / sie an FotografInnen oder fotopia spendet.
Wir starten also den Betrieb erst einmal spendenfinanziert und schauen, ob sich das trägt. Ergänzend wird es Spendenaufrufe zu konkreten Zielen wie der Umsetzung neuer Funktionen geben. Finanzkräftige Organisationen werden gebeten, ihren Möglichkeiten entsprechend aufzukommen.

## Wie können Echtheit und Herkunft der Bilder überprüft werden?
Die Aufbereitung der Bilder wird durch die fotopia-Community getragen. Fotos, die falsche Tatsachen zeigen, werden in diesem Prozess markiert und ggf. aussortiert. Das Design und ein Bewertungssystem wird die UploaderInnen dazu anhalten, aussagekräftige Bildunterschriften und Schlagwörter zu vergeben. Zudem sind eine automatische Prüfung der Metadaten auf Plausibilität und Warnmeldungen bei Pixelverschiebungen (Bildmanipulation) perspektivisch geplant.

## Wie gewährleistet ihr eine journalistische und ästhetische Qualität der Bilder?
Fotopia.cc lebt von den NutzerInnen. Wir bieten der Community ein Bewertungssystem an, mit dem Bildqualität, Verschlagwortung, journalistische Standards usw. eingeschätzt werden. Jenseits der Bewertung wird die Kommentar-Funktion helfen, den gegenseitigen Austausch und eine konstruktive Kritik zu pflegen.

## Verlieren die FotografInnen die Rechte an ihren Bildern?
Nein, das können die FotografInnen selbst entscheiden. Die Lizenz BY-NC-SA bewahrt beispielsweise den UrheberInnen im Gegensatz zur freien Veröffentlichung (CC0) einen Großteil ihrer Rechte, zum Beispiel das auf eine parallele kommerzielle Verwertung. Nur auf Wunsch kann auch hier verzichtet und das Werk zur kostenlosen Nutzung für kommerzielle Zwecke freigegeben werden.

## Warum sollte eure Suchfunktion bessere Treffer liefern als "normale" Suchmaschinen?
Bei fotopia.cc werden die Fotos im Upload-Prozess verschlagwortet. Wir stellen den UploaderInnen dafür einfache und schnelle Werkzeuge zur Verfügung. Zudem werden ausgewählte Aufnahmeparameter (Blende, Verschlusszeit, Zeit, Ort, etc.) anhand der Metadaten importierbar sein. Dies ermöglicht eine genaue Bildrecherche. Bei unzureichend dokumentierten Bildern kann durch die fotopia-Community nachgearbeitet werden.

## Wie steht ihr zu Projekten wie Umbruch-Bildarchiv, Arbeiterfotografie und r-mediabase?
Wir fühlen uns ebenfalls progressiven Idealen verpflichtet und sind solidarisch. Fotopia.cc soll die genannten Projekte ergänzen. Nachdem ein Account erstellt ist, kann jedeR seine/ihre Fotos zur Verfügung stellen. JedeR kann sie von überall und immer herunterladen und hat eine digitale Kopie. Fotopia.cc ist eine große digitale Gemeinschaft kritisch denkender Menschen im Web 2.0 - und Teil der Creative-Commons-Idee.
