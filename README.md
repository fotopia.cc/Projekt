# Projekt fotopia.cc

Dieses Repository dient zur Verwaltung der internen und öffentlichen Dokumente vom fotopia e. V. 
Das Tool dient zur Zeit in erster Linie der Transparenz, weitere Inhalte verwalten wir zur Zeit noch intern.
Wir versuchen, in Zukunft alle Inhalte öffentlich zu verwalten und auch der Community die einfache Mithilfe zu erlauben.
Bis dahin werden viele Inhalte noch unzugänglich oder zensiert sein, denn die Veröffentlichung von über zehn Jahren Altbestand ist eine Menge Arbeit
 - und vielleicht auch nicht überall notwendig, vielleicht ist auch gerade mal Zeit, um etwas auszumisten.


## Meta-Ordner

Der ist gedacht für alle Dinge, die für das Projekt selbst und den Start der Plattform nötig sind. FAQ, AGB, Bewerbungs- und Vorstellungstexte und alles in dieser Richtung.
Mithilfe zur Verbesserung der Texte ist gerne gesehen.


## Verein-Ordner

Alles was zur Vereinsarbeit von fotopia gehört, landet hier. Ihr könnt unsere Satzung und die aktuelle Kontoführung lesen. 
Achtung: Es beteht kein Anspruch auf Vollständigkeit und Richtigkeit. Vieles fehlt, ist unvollständig, eventuell aus Gründen des Datenschutzes zensiert.
Daran arbeiten wir also noch und versuchen, hier in Zukunft gleich auf eine öffentliche Arbeitsweise zu setzen.
